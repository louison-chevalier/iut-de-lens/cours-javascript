/* EXERCICE 1 */



//BASE
console.log("\n ")
console.log("------BASE------")
let t = [10, -3, 0, 12]
t.sort((v1,v2) => v1 - v2)
console.log("Après tri : " + t +"\n ")



//QUESTION 1
console.log("------QUESTION 1------")
let b1 = t.every(v => v >= 0)
console.log("Chaque valeur du tableau est positive ? " + b1 +"\n ")



//QUESTION 2
console.log("------QUESTION 2------")
let b2 = t.filter(v => v >= 10)
console.log("Valeur(s) supérieures à 10 : " + b2 +"\n ")



//QUESTION 3
console.log("------QUESTION 3------")
let b3 = t.map(v => v * 10)
let text = "Multiplier tableau x10 = "
for (b of b3){
    text += " "+ b +" |"
}
console.log(text +"\n ")



//A PROPOS DES OBJETS
console.log("------A PROPOS DES OBJETS------")
let obj = { } //Objet vide
obj.x = 10 // Ajout d'un champ
console.log(obj)
let obj1 = {color : "blue"} //Objet avec un champ
console.log(obj1)
console.log("\n ")



//QUESTION 4
console.log("------QUESTION 4------")
let alice = {name : "alice", age : "20" }
let john = {name : "john", age : "13" }
let persons = [alice, john]
persons.push({name : "jean-pierre", age : "56"})
console.log("Personnes :"+"\n", persons)
persons.sort((p1, p2) => p1.age - p2.age)
console.log("Personnes après tri (age):"+"\n", persons)

function compareNames(p1, p2) {
    if (p1.name < p2.name){
        return -1;
    }
    if (p1.name >p2.name){
        return 1;
    }
    return 0;
}

persons.sort(compareNames)
console.log("Personnes après tri (nom):"+"\n", persons)


let nomsMajeurs2 = persons.filter(p => p.age >=18).map(p => p.name)
console.log(nomsMajeurs2)



/* EXERCICE 3 */
// Ecrire une classe 'Animal' avec un constructeur permettant de définir le nom de l'animal et une méthode 'shout' affichant un simple message dans la console. Tester la classe.
// On modifie la classe pour intégrer un deuxième champ 'scream' indiquant le cri de l'animal. Ecrire une sous-classe 'Cow', et tester.
// Etoffer la hiérarchie de classes, en redéfinissant au moins une méthode.


console.log("\n EXERCICE 3 \n ");

class Animal{
    constructor(name, scream){
        this.name = name
        this.scream = scream
    }

    shout(){
        console.log("Ceci est un(e) " + this.name + " qui " + this.scream + " ")
    }
}

class Cow extends Animal{
    constructor(name) {
        super(name, "meugle");
    }
}

let a = new Animal("chien", "aboit")
a.shout()

let m = new Cow("vache")
m.shout();